project_boost
Tasks:
- [x] Add lives system
    - [x] Program the mechanic
    - [x] Add UI life counter
- [x] Add Fuel mechanic
    - [x] Program the mechanic
    - [x] Add UI fuel gauge
    - [x] Fine tune
- [x] Make level 3 easier
- [x] Add levels where the fuel mechanic is the focus
    - [x] Level 4: Introduction to the fuel mechanic.
    - [x] Level 5: Serpentine level.
- [x] Resolve the collision issue with fuel pickup items