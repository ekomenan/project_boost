﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelPickup : MonoBehaviour {

	[SerializeField][Tooltip("Amount of fuel refilled.")]float refill= 500.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround(transform.position, Vector3.up, 50 * Time.deltaTime);
	}

	// void OnCollisionEnter(Collision collision){
	// 	if(collision.gameObject.tag=="Rocket Ship"){
	// 		//Rocket rocketScript= collision.gameObject.GetComponent<Rocket>();
	// 		//rocketScript.FillFuel();
	// 		gameObject.SetActive(false);
	// 	}
		
	// }
}
