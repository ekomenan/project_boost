﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Rocket : MonoBehaviour {

	[SerializeField] [Tooltip("Rotation coeficient")]float rcsThrust= 100f;
	[SerializeField] [Tooltip("Thrust coeficient")]float mainThrust= 10f;
	[SerializeField] [Tooltip("Delay before a level is loaded")]float levelLoadDelay= 2f;
	[SerializeField][Tooltip("Initial fuel level")] float initFuel= 1000f;
	[SerializeField][Tooltip("Current level of fuel")]float fuel;
	[SerializeField][Tooltip("Rate at which fuel is lost")]float fuelLossRate=20.0f;
	[SerializeField][Tooltip("Amount of fuel refilled")]float refill=500.0f;

	[SerializeField] [Tooltip("Number of lives at the beginning of a level.")]private int startingLives=3;

	[SerializeField][Tooltip("Current number of lives")]static int lives=3;

	[SerializeField] AudioClip mainEngine;
	[SerializeField] AudioClip deathSound;
	[SerializeField] AudioClip successSound;

	[SerializeField] ParticleSystem mainEngineParticles;
	[SerializeField] ParticleSystem deathParticles;
	[SerializeField] ParticleSystem successParticles;

	Rigidbody rb;
	AudioSource audioSource;
	bool canCollide;
	[SerializeField]public bool fuelSystemActivated=false;

	[SerializeField] Text livesTextBox;
	[SerializeField] Text fuelTextBox;

	enum State{Alive, Dying, Transcending};
	State state= State.Alive;

	// Use this for initialization
	void Start () {
		rb= GetComponent<Rigidbody> ();
		audioSource = GetComponent<AudioSource> ();
		canCollide = true;
		livesTextBox.text= "Lives: "+ lives;
		//If the fuel system is activated, fill the fuel gauge, then display it to the GUI.
		if(fuelSystemActivated){
			fuel= initFuel;
			fuelTextBox.text= "Fuel: "+FormatFuelLevel(fuel)+"/100";
		}

	}
	
	// Update is called once per frame
	void Update () {
		if(state== State.Alive){
			if(fuelSystemActivated){
				RespondToThrustInputFuel();
			}
			else{
				RespondToThrustInput ();
			}
			RespondToRotationInput();
		}
		if(Debug.isDebugBuild){	//Only on debug builds and through the editor.
			RespondToLevelInput ();
			RespondToCollisionInput();
		}

	}
	void OnCollisionEnter(Collision collision){
		if(state!=State.Alive || !canCollide){	//If player collides with a hazard or the goal, prevent any other collision messages.
			return;
		}
		switch(collision.gameObject.tag){
		case "Friendly":
			if(fuelSystemActivated){
				//Completely refuel the rocket if the player lands on a blue platform.
				FillFuel(initFuel);
			}
			break;
		case "Finish":
			StartSuccessSequence ();
			break;
		default:
			StartDeathSequence ();
			break;
		}

	}
	void OnTriggerEnter(Collider other)
	{
		if(state!=State.Alive || !canCollide){	//If player collides with a hazard or the goal, prevent any other collision messages.
			return;
		}
		if(other.gameObject.tag=="Fuel Pickup"){
			if(fuelSystemActivated){
				//Partially refill the gauge.
				FillFuel(refill);
			}
			other.gameObject.SetActive(false);
		}
	}
	private void StartSuccessSequence(){
		state=State.Transcending;
		audioSource.Stop ();
		mainEngineParticles.Stop ();
		audioSource.PlayOneShot (successSound);
		successParticles.Play ();
		//Will call the LoadFirstScene method in levelLoadDelay seconds
		Invoke ("LoadNextScene", levelLoadDelay);
	}

	private void StartDeathSequence(){
		state = State.Dying;
		audioSource.Stop ();
		mainEngineParticles.Stop ();
		audioSource.PlayOneShot (deathSound);
		deathParticles.Play ();
		//Will call the LoadFirstScene method in levelLoadDelay seconds 
		if(lives==0)
			Invoke ("LoadFirstScene", levelLoadDelay);
		else{
			lives--;
			//print(lives);
			Invoke("ReloadCurrentScene", levelLoadDelay);
		}
	}

	private void LoadFirstScene(){
		//Restore lives to their initial count.
		lives= 3;
		SceneManager.LoadScene (0);
	}

	private void LoadNextScene(){ //Goes to the next level. If at the final level, loop back to the first level.
		int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		int nextSceneIndex;
		if(currentSceneIndex<SceneManager.sceneCountInBuildSettings-1){
			nextSceneIndex = currentSceneIndex + 1;
		}
		else{
			nextSceneIndex = 0;
		}
		//Restore lives to their initial count.
		lives= 3;
		SceneManager.LoadScene (nextSceneIndex);
	}

	private void ReloadCurrentScene(){ //Reload the current level
		int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		int nextSceneIndex;
		if(currentSceneIndex<SceneManager.sceneCountInBuildSettings-1){
			nextSceneIndex = currentSceneIndex + 1;
		}
		else{
			nextSceneIndex = 0;
		}
		SceneManager.LoadScene (currentSceneIndex);
	}
	//Handles thrust controls.
	private void RespondToThrustInput () {
		if (Input.GetKey (KeyCode.Space)) {
			ApplyThrust ();
		}
		else {
			audioSource.Stop ();
			mainEngineParticles.Stop ();
		}
		rb.freezeRotation = false;
	}

	private void RespondToThrustInputFuel(){
		if (Input.GetKey (KeyCode.Space)&& (fuel>0.0f)) {
			//Lose fuel, update the UI, then thrust.
			fuel=fuel-fuelLossRate;
			fuelTextBox.text= "Fuel: "+FormatFuelLevel(fuel)+"/100";
			ApplyThrust ();
		}
		else {
			audioSource.Stop ();
			mainEngineParticles.Stop ();
		}
		rb.freezeRotation = false;
	}
	private void ApplyThrust ()
	{
		rb.freezeRotation = true;
		rb.AddRelativeForce (Vector3.up * mainThrust*Time.deltaTime);
		if (!audioSource.isPlaying) {
			audioSource.PlayOneShot (mainEngine);
		}
		mainEngineParticles.Play ();
	}

	//Handles rotation controls
	private void RespondToRotationInput(){
		
		if(Input.GetKey (KeyCode.A)||Input.GetKey(KeyCode.D)) 	//Assume manual rotation controls if there is an input.
			rb.freezeRotation = true;	

		float rotationThisFrame = rcsThrust * Time.deltaTime;
		if(Input.GetKey (KeyCode.A)){
			transform.Rotate (Vector3.forward*rotationThisFrame);
		}
		else if(Input.GetKey (KeyCode.D)){
			transform.Rotate (Vector3.back*rotationThisFrame);
		}

		rb.freezeRotation = false; //Let physics control rotation.
	}
	private void RespondToLevelInput(){
		if(Input.GetKeyDown (KeyCode.L)){
			LoadNextScene ();
		}
	}
	private void RespondToCollisionInput(){
		if(Input.GetKeyDown (KeyCode.C)){
			canCollide = !canCollide;
		}
	}
	//Returns the current fuel level as a fraction of 100 rounded down;
	private float FormatFuelLevel(float input){
		return Mathf.Floor((input/initFuel)*100.0f);
	}

	public void FillFuel(float refill){
		fuel+=refill;
		if(fuel>initFuel){
			fuel= initFuel;
		}
		fuelTextBox.text= "Fuel: "+FormatFuelLevel(fuel)+"/100";
	}
}
